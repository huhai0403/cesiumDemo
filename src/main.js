import Vue from 'vue'
import App from './App.vue'
import router from './router'
// cesium
import 'cesium/Widgets/widgets.css'
Vue.config.productionTip = false;
import com from './components/common'
import axios from 'axios'
Vue.prototype.$axios = axios;
//全局方法
Vue.prototype.globalEv = com;
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
